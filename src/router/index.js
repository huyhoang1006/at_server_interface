/* eslint-disable */
import Vue from 'vue'
import store from '@/store'
import VueRouter from 'vue-router'
import Layout from '@/layout/index.vue'
import LoginView from '@/views/login/Login.vue'
import Manage from '@/views/manageView/ManageView.vue'
import UserDetail from '@/views/userDetailView/UserDetailView.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        component: Layout,
        redirect: {name: 'manage'},
        children: [
            {
                path: '/manage',
                name: 'manage',
                meta: {title: 'manage'},
                component: Manage
            },
            {
                path: '/login',
                name: 'login',
                meta: {title: 'login'},
                component: LoginView
            },
            {
                path: '/manage-user',
                name: 'manage-user',
                meta: {title: 'user detail'},
                component: UserDetail
            },
        ]
    }
]

const router = new VueRouter({
    mode: process.env.IS_ELECTRON ? 'hash' : 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach(async (to, from, next) => {
    try {
        const isAuthenticated = store.state.isAuthenticated

        if (isAuthenticated) {
            if (to.path === '/login') {
                if (from.path !== '/') {
                    next({ path: '/' });
                } else {
                    next('/manage')
                    // next('/manage'); // Ngăn điều hướng thừa nếu đã ở trang chủ
                }
            } else {
                next()
            }
        } else {
            // if (['/login'].indexOf(to.path) !== -1) {
            //     next()
            // } else {
            //     next(`/login?redirect=${to.path}`)
            // }
            // Nếu người dùng chưa đăng nhập
            if (to.path === '/login') {
                if(from.path !== '/login') {
                    next();
                } else {
                    next(false);
                }
                // next(); // Cho phép điều hướng đến /login
            } else {
                // Điều hướng đến /login kèm theo đường dẫn gốc để điều hướng lại sau khi đăng nhập
                if (to.path !== '/login') {
                    next(`/login?redirect=${to.path}`);
                } else {
                    next(false); // Ngăn điều hướng thừa nếu đã ở trang /login
                }
            }
        }
    } catch(e) {
        console.log(e)
    }
})

export default router
