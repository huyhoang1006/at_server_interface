/* eslint-disable */
import {mapState} from 'vuex'
import * as manuAPI from '@/api/manuAPI/manuAPI.js'
export default {
    data() {
        return {
            properties: {
                id: '',
                asset: "",
                asset_type: '',
                serial_no: '',
                manufacturer: '',
                manufacturer_type: '',
                manufacturing_year: '',
                asset_system_code: '',
                apparatus_id: '',
                feeder: '',
                date_of_warehouse_receipt: '',
                date_of_delivery: '',
                date_of_production_order: '',
                date_of_warehouse_delivery: '',
                progress: '',
                standard: '',
                thermal_meter: '',
                comment: '',
                type_disable : ''
            },
            winding_configuration: {
                phases: '1',
                vector_group: {
                    prim: '',
                    sec: {
                        I: '',
                        Value: ''
                    },
                    tert: {
                        I: '',
                        Value: '',
                        accessibility: ''
                    }
                },
                vector_group_custom: '',
                unsupported_vector_group: ''
            },
            ratings: {
                rated_frequency: '50',
                rated_frequency_custom: '50',
                voltage_ratings: [],
                voltage_regulation: [],
                power_ratings: [],
                current_ratings: [],
                short_circuit: {
                    ka: {
                        value: '',
                        unit: 'kA'
                    },
                    s: ''
                }
            },
            impedances: {
                ref_temp: 75,
                prim_sec: [],
                prim_tert: [],
                sec_tert: [],
                zero_sequence_impedance: {
                    base_power: {
                        value: '',
                        unit: 'MVA'
                    },
                    base_voltage: {
                        value: '',
                        unit: 'kV'
                    },
                    zero_percent: {
                        z_percent : '',
                        prim : '',
                        sec : ''
                    }
                }
            },
            others: {
                category: '',
                status: '',
                tank_type: '',
                insulation_medium: '',
                oil_type: '',
                insulation: {
                    key: 'Weight',
                    weight: '',
                    volume: ''
                },
                total_weight: '',
                winding: {
                    prim: 'Copper',
                    sec: 'Copper',
                    tert: 'Copper'
                }
            },
            tapChangers: {
                id: '',
                mode: '',
                serial_no: '',
                manufacturer: '',
                manufacturer_type: '',
                winding: '',
                tap_scheme: '',
                no_of_taps: '0',
                voltage_table: []
            },
            bushings_config: {
                id: null,
                asset_type: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    NameOfPos : {
                        prim : {},
                        sec : {},
                        tert : {}
                    },
                    DataShow : {
                        prim : {
                            fi : true,
                            se : true,
                            th : false,
                            fo : false
                        },
                        sec : {
                            fi : true,
                            se : false,
                            th : false,
                            fo : false
                        },
                        tert : {
                            fi : false,
                            se : false,
                            th : false,
                            fo : false
                        }
                    }
                },
                serial_no: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                manufacturer: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                manufacturer_type: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                manufacturer_year: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                insull_level: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                voltage_gr: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                max_sys_voltage: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        o: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                rate_current: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                df_c1: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                cap_c1: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                df_c2: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                cap_c2: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                },
                insulation_type: {
                    prim: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    sec: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    },
                    tert: {
                        fi: '',
                        se: '',
                        th: '',
                        fo: ''
                    }
                }
            },
            config: '',
            disabled : false
        }
    },
    computed: mapState(['selectedLocation', 'selectedAsset']),
    methods: {
        async loadData(data) {
            this.properties = data
            try {
                let rs = await manuAPI.getManuByType(this.title)
                this.manufacturerCustom = rs
            } catch(e) {
                console.log(e)
                this.$message.error("Cannot load manufacturer data")
            }
            
        },
        async loadDataEdit(data) {
            await this.mappingData(data)
            try {
                let rs = await manuAPI.getManuByType(this.title)
                this.manufacturerCustom = rs
            } catch(e) {
                console.log(e)
                this.$message.error("Cannot load manufacturer data")
            }
        },
        getAssetType(data) {
            if(data.NameOfPos == undefined || data.DataShow == undefined ) {
                if(data.NameOfPos == undefined) {
                    let temp = {
                        NameOfPos : {
                            prim : {},
                            sec : {},
                            tert : {}
                        }
                    }
                    data = Object.assign(data, temp)
                }
                if(data.DataShow == undefined) {
                    let dataShow = {
                        DataShow : {
                            prim : {
                                fi : true,
                                se : true,
                                th : false,
                                fo : false
                            },
                            sec : {
                                fi : true,
                                se : false,
                                th : false,
                                fo : false
                            },
                            tert : {
                                fi : false,
                                se : false,
                                th : false,
                                fo : false
                            }
                        }
                    }
                    data = Object.assign(data, dataShow)
                }
                return data
            } else {
                return data
            }
        },
        async saveAsset() {
            if (this.mode === this.$constant.ADD || this.mode === this.$constant.DUP) {
                await this.insertAsset()
            } else {
                await this.updateAsset()
            }
        },
        async insertAsset() {
            const properties = this.properties
            const winding_configuration = this.winding_configuration
            const ratings = this.ratings
            const impedances = this.impedances
            const others = this.others
            const asset = {
                properties,
                winding_configuration,
                ratings,
                impedances,
                others
            }
            const locationId = this.selectedLocation[0].id
            const rs = await window.electronAPI.insertAsset(locationId, asset, this.tapChangers, this.bushings_config)
            if (rs.success) {
                this.$message({
                    type: 'success',
                    message: 'Insert completed'
                })
                this.$router.push({name: 'manage'})
            } else {
                this.$message.error(rs.message)
            }
        },
        async updateAsset() {
            const properties = this.properties
            const winding_configuration = this.winding_configuration
            const ratings = this.ratings
            const impedances = this.impedances
            const others = this.others
            const asset = {
                properties,
                winding_configuration,
                ratings,
                impedances,
                others
            }
            const rs = await window.electronAPI.updateAsset(asset, this.tapChangers, this.bushings_config)
            if (rs.success) {
                this.$message({
                    type: 'success',
                    message: 'Update completed'
                })
            } else {
                this.$message.error(rs.message)
            }
        },
        async getData() {
            let impedances = JSON.parse(JSON.stringify(this.impedances))
            let { base_power, base_voltage, zero_percent } = impedances.zero_sequence_impedance;
            delete impedances.zero_sequence_impedance;
            impedances.base_power = base_power;
            impedances.base_voltage = base_voltage;
            impedances.zero_percent = zero_percent;
            this.ratings.max_short_circuit_current_ka = this.ratings.short_circuit.ka
            this.ratings.max_short_circuit_current_s = this.ratings.short_circuit.max_short_circuit_current_s
            this.others.insulation_key = this.others.insulation.key
            this.others.insulation_volume = this.others.insulation.volume
            this.others.insulation_weight = this.others.insulation.weight

            let data = {
                properties : this.properties,
                winding_configuration : this.winding_configuration,
                ratings : this.ratings,
                impedances : impedances,
                others : this.others,
                tapChangers : this.tapChangers,
                bushings_config : this.bushings_config
            }
            return data
        },
        async mappingData(data) {
            const dataAsset = data.asset
            console.log(dataAsset)
            this.properties = {
                id: dataAsset.id,
                asset: dataAsset.asset,
                asset_type: dataAsset.asset_type,
                serial_no: dataAsset.serial_no,
                manufacturer: dataAsset.manufacturer,
                manufacturer_type: dataAsset.manufacturer_type,
                manufacturing_year: dataAsset.manufacturing_year,
                asset_system_code: dataAsset.asset_system_code,
                apparatus_id: dataAsset.apparatus_id,
                feeder: dataAsset.feeder,
                comment: dataAsset.comment,
            }
    
            this.winding_configuration = {
                phases: dataAsset.phases,
                vector_group: JSON.parse(dataAsset.vector_group),
                vector_group_custom: dataAsset.vector_group_custom,
                unsupported_vector_group: dataAsset.unsupported_vector_group
            }
    
            this.ratings = {
                rated_frequency: dataAsset.rated_frequency,
                rated_frequency_custom: (dataAsset.rated_frequency_custom === undefined || dataAsset.rated_frequency_custom === null) ? "" : dataAsset.rated_frequency_custom,
                voltage_ratings: JSON.parse(dataAsset.voltage_ratings),
                voltage_regulation: JSON.parse(dataAsset.voltage_regulation),
                power_ratings: JSON.parse(dataAsset.power_ratings),
                current_ratings: JSON.parse(dataAsset.current_ratings),
                short_circuit: {
                    ka: (dataAsset.max_short_circuit_current_ka === undefined || dataAsset.max_short_circuit_current_ka === null) ? {value: '', unit: 'kA'} : JSON.parse(dataAsset.max_short_circuit_current_ka),
                    s: dataAsset.max_short_circuit_current_s
                }
            }
    
            this.impedances = {
                ref_temp: dataAsset.ref_temp,
                prim_sec: JSON.parse(dataAsset.prim_sec),
                prim_tert: JSON.parse(dataAsset.prim_tert),
                sec_tert: JSON.parse(dataAsset.sec_tert),
                zero_sequence_impedance: {
                    base_power: JSON.parse(dataAsset.base_power),
                    base_voltage: JSON.parse(dataAsset.base_voltage),
                    zero_percent: (dataAsset.zero_percent == null || dataAsset.zero_percent === '') ? {z_percent : '', prim : '', sec: ''} : JSON.parse(dataAsset.zero_percent)
                }
            }
    
            this.others = {
                category: dataAsset.category,
                status: dataAsset.status,
                tank_type: dataAsset.tank_type,
                insulation_medium: dataAsset.insulation_medium,
                oil_type: dataAsset.oil_type,
                insulation: {
                    key: dataAsset.insulation_key,
                    weight: dataAsset.insulation_weight,
                    volume: dataAsset.insulation_volume
                },
                total_weight: dataAsset.total_weight,
                winding: JSON.parse(dataAsset.winding)
            }
            const dataTapChanger = data.tap_changer
            this.tapChangers = {
                id: dataTapChanger.id,
                mode: dataTapChanger.mode,
                _mode: dataTapChanger._mode,
                serial_no: dataTapChanger.serial_no,
                manufacturer: dataTapChanger.manufacturer,
                manufacturer_type: dataTapChanger.manufacturer_type,
                winding: dataTapChanger.winding,
                _winding: dataTapChanger._winding,
                tap_scheme: dataTapChanger.tap_scheme,
                no_of_taps: dataTapChanger.no_of_taps,
                voltage_table: JSON.parse(dataTapChanger.voltage_table)
            }
            const dataBushings = data.bushings
            this.bushings_config = {
                id: dataBushings.id,
                asset_type: this.getAssetType(JSON.parse(dataBushings.asset_type)),
                serial_no: JSON.parse(dataBushings.serial_no),
                manufacturer: JSON.parse(dataBushings.manufacturer),
                manufacturer_type: JSON.parse(dataBushings.manufacturer_type),
                manufacturer_year: JSON.parse(dataBushings.manufacturer_year),
                insull_level: JSON.parse(dataBushings.insull_level),
                voltage_gr: JSON.parse(dataBushings.voltage_gr),
                max_sys_voltage: JSON.parse(dataBushings.max_sys_voltage),
                rate_current: JSON.parse(dataBushings.rate_current),
                df_c1: JSON.parse(dataBushings.df_c1),
                cap_c1: JSON.parse(dataBushings.cap_c1),
                df_c2: JSON.parse(dataBushings.df_c2),
                cap_c2: JSON.parse(dataBushings.cap_c2),
                insulation_type: JSON.parse(dataBushings.insulation_type)
            }
        },
    }
}